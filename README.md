# iOS ONprint Light SDK Sample

In no time, create an application to get information of enriched images by taking picture.

## Introduction

This [Sample API][1] shows you how to use basic functionalities of ONprint Light SDK.
By using [ONprint API][2], it provides you an interface for individual camera
connected to an iOS device in order
to get informations from picture.

In this sample two modes are available:
- **Picture mode**: getting picture data associated to a picture.
- **Enriched Image mode**: getting more informations (title, actions, etc.) about an enriched image.

Picture recognition unfolds in three steps:
1. taking a picture
1. sending this picture to ONprint server
1. getting associated data if the picture is identified

Furthermore, it allows you to record usage stats.

## Pre-requisites

- iPhone and iPad
- xCode 10
- Swift 4.2


## Screenshots

<img src="screenshots/main.png" height="400" alt="Screenshot"/>

## SDK Integration

To start, install the SDK for your own project by following the [install doc][3].
<br/><br/>
Then, to integrate ONprint functions, please consult the [developer guide][4].


### Support

If you've found an error in this sample or if you have any question,
please send us an email to: mobile@onprint.com


### License

© 2018 by LTU Tech. All Rights Reserved.

[1]:https://gitlab.com/onprint_public/iOS_ONprint_Light_SDK_Sample
[2]: http://developer.onprint.com/api.html
[3]: https://gitlab.com/onprint_public/iOS_ONprint_Light_SDK_Sample/blob/master/INSTALL.md
[4]: https://gitlab.com/onprint_public/iOS_ONprint_Light_SDK_Sample/blob/master/DEV.md

