# SDK Installation


Here are the steps to use the Light SDK.


## Build Installation


### Add light SDK package


You first have to add the ONprint light SDK to your project.
To do this you'll have to copy ["OnprintSDKLight.framework"][1]
directory inside your "Frameworks" directory.


### Add Light SDK package

Link OnprintSDKLight.framework in "Embedded Binaries" section.


## Swift Initialisation

Inside your project ```AppDelegate```, add in the upper part:

``` java
import OnprintSDKLight
``` 


### SDK intialization

To init ONprint SDK you need a apiKey which is a character chain allowing you to use ONprint API services with your dedicating data.

``` java
OnprintLight.setApiKey(apiKey)
```

To obtain your personal API Key, please contact mobile@onprint.com


## Dev

Now it's time... to code!
Please follow -> the [developer guide][2]


[1]: https://gitlab.com/onprint_public/iOS_ONprint_Light_SDK_Sample/tree/master/ONprintSdkSample/Frameworks/OnprintSDKLight.framework
[2]: https://gitlab.com/onprint_public/iOS_ONprint_Light_SDK_Sample/blob/master/DEV.md
