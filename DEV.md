# iOS ONprint light SDK Developer Guide

Welcome to the ONprint developer guide.<br/><br/>
This document lists all the main ONprint light SDK functions to explain you how
to build an iOS application using ONprint picture recognition.


## The five API functions

The  "Server" struct contains the following five mains SDK functionalities:


### > enrichedImage API function

This function is called with a numeric picture as parameter to get its associated Enriched Image object.

``` java
func enrichedImage(_ image: UIImage?, languageCode: String, completion: @escaping (Image?, OnprintError?) -> ())
```

### > enrichedImageById API function

This function is called with an Enriched Image ID as parameter to get its associated Enriched Image object.

``` java
public func enrichedImageById(_ imageId: String?, completion: @escaping (Image?, OnprintError?) -> ()) {
```

### > pictureById API function

This function is called with an Picture ID as parameter to get its associated Picture object.

``` java
public func pictureById(_ imageId: String?, completion: @escaping (Picture?, OnprintError?) -> ()) {
```

### > pictureSearch API function

This function is called with a numeric picture as parameter to get its associated Enriched Image objects.
Returned objects will be ordered by probability (score) detection.
It is also possible to pass a keywords list to filter the result.

``` java
public func pictureSearch(_ image: UIImage?, keywords: [String]?, completion: @escaping ([Picture]?, OnprintError?) -> ()) {
```

### > clicksWithSessionId API function (only for Enriched Image)

This function is called for statistic purpose.
For each user session you can record user actions.
Statistics can be consult in the ONprint admin console.

``` java
func clicksWithSessionId(_ sessionId: String, actionId: String, completion: @escaping (String?, Error?) -> ())
```


## Models


The following objects are available:

* Image (Enriched Image)
* Picture
* Action
* Button
* Text
* Icon
* Content
* Style


### > Image (Enriched Image)

You can get an Image by using the enrichedImage and enrichedImageById functions.
Here are the attribues contained inside:
* id: String
* order: Int
* title: String
* sessionId: String
* documentId: String
* titleStyle: Style
* autoTrigger: Bool
* languageCode: String
* bigThumbnailUrl: String



### > Picture

You can get a Picture by calling the pictureSearch and pictureById functions.
Here are the attribues contained inside:
* _objectId: String
* id: String
* name: String
* data: String
* organizationId: String
* bigThumbnailUrl: String
* sessionId: String
* recognitionScore: Double
* keywords: [String]

keywords is a String list.
