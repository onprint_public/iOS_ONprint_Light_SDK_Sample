//
//  CameraController.swift
//  OnprintSDK
//
//  Created by OnPrint on 12/07/2017.
//  Copyright © 2017 ONprint. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation

public class CameraManager {
    var captureSession: AVCaptureSession?
    
    var rearCamera: AVCaptureDevice?
    
    var currentCameraPosition: CameraPosition?

    var rearCameraInput: AVCaptureDeviceInput?
    var photoOutput: AVCaptureStillImageOutput?
    
    var previewLayer: AVCaptureVideoPreviewLayer?
    
    var flashMode = AVCaptureDevice.FlashMode.off
    
    public init() {}
    
    public func configureInView(_ view: UIView) {
        self.prepare {(error) in
            if let error = error { print(error) }
            try? self.displayPreview(on: view)
        }
    }
    
    public func toggleFlash() -> Bool {
        if let device = self.rearCamera, device.hasTorch { //AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo), device.hasTorch {
            do {
                try device.lockForConfiguration()
                let torchOn = !device.isTorchActive
                try device.setTorchModeOn(level: 1.0)
                device.torchMode = torchOn ? .on : .off
                device.unlockForConfiguration()
                return torchOn
            } catch let error as NSError {
                print("toggleFlash error :: \(error.localizedDescription)")
                return false
            }
        }
        return false
    }
    
    public func capturePicture(completion: @escaping (UIImage?, Error?) -> Void) {
        guard let captureSession = captureSession, captureSession.isRunning else { completion(nil, CameraControllerError.captureSessionIsMissing); return }
        
        let connection = self.photoOutput?.connection(with: AVMediaType.video)
        self.photoOutput?.captureStillImageAsynchronously(from: connection!, completionHandler: { (cmsSampleBuffer, error) in
            guard let sampleBuffer = cmsSampleBuffer, let data = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer) else {
                completion(nil, CameraControllerError.unknown)
                return
            }
            let image = UIImage(data: data)
            completion(image, nil)
        })
    }
}

extension CameraManager {
    
    fileprivate func prepare(completionHandler: @escaping (Error?) -> Void) {
        func createCaptureSession() {
            self.captureSession = AVCaptureSession()
        }
        
        func configureCaptureDevices() throws {
            if #available(iOS 10.0, *) {
                let session = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .unspecified)
                //guard let cameras = (session.devices.flatMap { $0 }), !cameras.isEmpty else { throw CameraControllerError.noCamerasAvailable }
                let cameras = (session.devices.compactMap { $0 })
                
                for camera in cameras {
                    if camera.position == .back {
                        self.rearCamera = camera
                        
                        try camera.lockForConfiguration()
                        camera.focusMode = .continuousAutoFocus
                        camera.unlockForConfiguration()
                    }
                }
            } else {
                //guard let cameras = AVCaptureDevice.devices(for: AVMediaType.video) else { throw CameraControllerError.noCamerasAvailable }
                let cameras = AVCaptureDevice.devices(for: AVMediaType.video)
                for camera in cameras {
                    if (camera as AnyObject).position == AVCaptureDevice.Position.back {
                        self.rearCamera = camera

                        try self.rearCamera?.lockForConfiguration()
                        if self.rearCamera?.isFocusModeSupported(.continuousAutoFocus) == true {
                            self.rearCamera?.focusMode = .continuousAutoFocus
                        } else if self.rearCamera?.isFocusModeSupported(.autoFocus) == true  {
                            self.rearCamera?.focusMode = .autoFocus
                        } else if self.rearCamera?.isFocusModeSupported(.locked) == true  {
                            self.rearCamera?.focusMode = .locked
                        }
                        self.rearCamera?.unlockForConfiguration()
                    }
                }
            }
            
        }
        
        func configureDeviceInputs() throws {
            guard let captureSession = self.captureSession else { throw CameraControllerError.captureSessionIsMissing }
            
            if let rearCamera = self.rearCamera {
                self.rearCameraInput = try AVCaptureDeviceInput(device: rearCamera)
                
                if captureSession.canAddInput(self.rearCameraInput!) { captureSession.addInput(self.rearCameraInput!) }
                
                self.currentCameraPosition = .rear
            }
            else { throw CameraControllerError.noCamerasAvailable }
        }
        
        func configurePhotoOutput() throws {
            guard let captureSession = self.captureSession else { throw CameraControllerError.captureSessionIsMissing }
            
            self.photoOutput = AVCaptureStillImageOutput()
            self.photoOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            if captureSession.canAddOutput(self.photoOutput!) { captureSession.addOutput(self.photoOutput!) }

            captureSession.startRunning()
        }
        
        DispatchQueue(label: "prepare").async {
            do {
                createCaptureSession()
                try configureCaptureDevices()
                try configureDeviceInputs()
                try configurePhotoOutput()
            }
                
            catch {
                DispatchQueue.main.async {
                    completionHandler(error)
                }
                
                return
            }
            
            DispatchQueue.main.async {
                completionHandler(nil)
            }
        }
    }
    
    fileprivate func displayPreview(on view: UIView) throws {
        guard let captureSession = self.captureSession, captureSession.isRunning else { throw CameraControllerError.captureSessionIsMissing }
        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        self.previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.previewLayer?.connection?.videoOrientation = .portrait
        
        view.layer.insertSublayer(self.previewLayer!, at: 0)
        self.previewLayer?.frame = view.bounds
    }

}

extension CameraManager {
    enum CameraControllerError: Swift.Error {
        case captureSessionAlreadyRunning
        case captureSessionIsMissing
        case inputsAreInvalid
        case invalidOperation
        case noCamerasAvailable
        case unknown
    }
    
    public enum CameraPosition {
        case rear
    }
}
