//
//  ResultsViewController.swift
//  ONprintSdkSample
//
//  Created by ONprint on 03/08/2018.
//  Copyright © 2018 ONprint. All rights reserved.
//

import UIKit
import OnprintSDKLight

class ResultsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var pictures: [Picture]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        tableView.reloadData()
    }
}

extension ResultsViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pictures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultsCell") as! ResultsCell
        cell.label.text = "Id: \(pictures[indexPath.row].id ?? "")\nName: \(pictures[indexPath.row].name ?? "")\nData: \(pictures[indexPath.row].data ?? "")\nKeywords: \(pictures[indexPath.row].keywords ?? [""])"
        return cell
    }
}

class ResultsCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
}
