//
//  CameraViewController.swift
//  ONprintSdkSample
//
//  Created by ONprint on 25/06/2018.
//  Copyright © 2018 ONprint. All rights reserved.
//

import UIKit
import OnprintSDKLight

class CameraViewController: UIViewController {
    
    let cameraManager = CameraManager()
    
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var capturePreviewView: UIView!
    @IBOutlet weak var takePictureButton: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cameraManager.configureInView(self.capturePreviewView)
        segmentedControl.setTitle(tr("enriched_image"), forSegmentAt: 0)
        segmentedControl.setTitle(tr("pictures_search"), forSegmentAt: 1)
    }

    @IBAction func takePicture(_ sender: Any) {
        self.cameraManager.capturePicture(completion: { (picture, error) in
            guard let picture = picture else {
                print(error ?? "Image capture error")
                return
            }
            self.showIndicatorView(true)
            
            if self.segmentedControl.selectedSegmentIndex == 0 {
                Server.shared.enrichedImage(picture) { (image, error) in
                    DispatchQueue.main.async{
                        if let image = image {
                            var title = ""
                            if let value = image.title { title = value }
                            else if let value = image.data { title = value }
                            let alertController = UIAlertController(title: nil, message: "\(tr("title")) : \(title)", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: tr("close"), style: .default, handler: nil))
                            self.present(alertController, animated: true, completion: nil)
                        } else if let error = error {
                            self.printError(error)
                        }
                        self.showIndicatorView(false)
                    }
                }
            } else {
                Server.shared.pictureSearch(picture, keywords: nil) { (pictures, error) in
                    DispatchQueue.main.async{
                        if let pictures = pictures {
                            let controller = self.storyboard?.instantiateViewController(withIdentifier: "ResultsViewController") as! ResultsViewController
                            controller.pictures = pictures
                            self.navigationController?.pushViewController(controller, animated: true)
                        } else if let error = error {
                            self.printError(error)
                        }
                        self.showIndicatorView(false)
                    }
                }
            }
        })
    }
    
    private func showIndicatorView(_ show: Bool) {
        DispatchQueue.main.async{
            self.indicatorView.isHidden = !show
            self.takePictureButton.isHidden = show
            self.navigationController?.setNavigationBarHidden(show, animated: true)
        }
    }
    
    private func printError(_ error: OnprintError) {
        DispatchQueue.main.async {
            let controller = UIAlertController(title: nil, message: error.localizedString, preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: tr("close"), style: .default, handler: nil))
            self.present(controller, animated: true, completion: nil)
        }
    }

}
