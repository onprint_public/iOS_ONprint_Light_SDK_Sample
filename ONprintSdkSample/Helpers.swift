//
//  Helpers.swift
//  ONprintSdkSample
//
//  Created by ONprint on 03/08/2018.
//  Copyright © 2018 ONprint. All rights reserved.
//

import Foundation

func tr(_ key: String) -> String {
    return Bundle.main.localizedString(forKey: key, value: nil, table: nil)
}
