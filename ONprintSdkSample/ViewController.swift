//
//  ViewController.swift
//  ONprintSdkSample
//
//  Created by ONprint on 25/06/2018.
//  Copyright © 2018 ONprint. All rights reserved.
//

import UIKit
import OnprintSDKLight

class ViewController: UIViewController {
    
    let cameraController = CameraController()
    
    @IBOutlet weak var capturePreviewView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cameraController.configureInView(self.capturePreviewView)
    }

    @IBAction func takePicture(_ sender: Any) {
        self.cameraController.capturePicture(completion: { (picture, error) in
            guard let picture = picture else {
                print(error ?? "Image capture error")
                return
            }
            
            let langCode = Locale.preferredLanguages[0]
            Server.enrichedImage(picture, languageCode: langCode) { (image, error) in
                guard let image = image else { return }
                let numberOfActions = image.actions?.count ?? 0
                let alertController = UIAlertController(title: "Enriched Image", message: "Nombre d'actions : \(numberOfActions)", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Fermer", style: .default, handler: nil)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
                print(image)
            }

        })
    }
}
